# Kaguya

Current leader of Shinto in Yamato.
Yongest leader also.
At the beginning of story her age is 15.

Previous leader nominated her as successor at the age of 10.
At this time she already had been known for her genius.
Most of her life was sheltered and therefore quite naive & ignorant.

## Appearance

* Long white hair;
* Height 155cm;
* Ceremonial white robes with orange & red ornaments;
* Purple cloak.

## Traits and other interesting things

* Genius, yet naive;
* Educated and loves to talk about history;
* Loves to teach;
* Dislikes impatience.

# Feallair

Abbot of Daor Monastery and father figure to protagonist of the story.

## Appearance

A bit oldish man with hint of gray hair.
He is always seen to wear his black robes.
Rather slim and quite tall (190cm).

## Traits and other interesting things

* Generally easy-going and kind.
* Particularly strict with his step-son.
* Secretive.
* Has great knowledge of history and magic.

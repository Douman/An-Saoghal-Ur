# Shinobu

Daughter of **Otomo** house.

Currently serves as attendant to Yamato's princess.

While her original goal in court to increase her father's influence,
she became very attached to **Shizuno** and genuinely loyal to her.

## Appearance

* Short(around shoulder) black hair;
* Height 174cm;
* Dark-red leather armour.

## Traits and other interesting things

* Easy-going;
* Very loyal to **Shizuno**;
* Kind demeanour;
* Despite her demeanour she can be pretty cruel, when necessary.


# Shizuno

Princess of Yamato.
She was living sheltered life most of the time.
Known as well-mannered and beautiful lady.

Thanks to her father, she has been trained to use bow.
As her father often encouraged her interests, she is quite fond of him.
Contrary to that her mother always believed that she should be raised as a proper lady and
due to that Shizuno often found herself displeased with mother.

Rather lonesome and considers as only friend her attendant **Shinobu**.

## Appearance

* Long black hair;
* Height 172cm;
* White kimono adorned by flowers.

## Traits and other interesting things

* Graceful and calm;
* Yamato nadeshiko;
* She despise liars due to being affected by court intrigues.
* Holds contempt for court and current state of affairs in empire.

# Characters

Main and notable characters

- [Ciarnight](./ciarnight.md)
- [Feallair](./feallair.md)
- [Kaguya](./kaguya.md)
- [Morgause](./morgause.md)
- [Shinobu](./Shinobu.md)
- [Shizuno](./shizuno.md)

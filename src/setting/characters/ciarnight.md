# Ciarnight

Ancient Sith Sage that ruled over prior to Sith fall.
In past, while being distant, she has been known to care for the world.

Since Sith fall she went in self-seclusion and forgot about world problems at all.
She is distrustful toward everyone, especially humans.

While being very intelligent she usually brush others opinion aside.
Even though **Ciarnight** is rather apathetic she is very prideful and is sure to point out that she is right.

## Appearance

Common Unseelie physics.

* Long gray hair;
* Short (168cm)
* Prefers dark clothes

## Traits and other interesting things

* Apathetic and cold;
* Over-powered mage - she is one of few who have enough power to destroy entire armies and cities;
* She doesn't like to over-work and tends to get rid of bothersome things;
* She has a hobby of writing books;
* Only mage that is capable to open paths to other dimensions/worlds.


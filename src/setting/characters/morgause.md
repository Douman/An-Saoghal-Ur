# Morgause

Princess and one of the potential candidates to throne of Leanait Empire.

## Appearance

* Long brown hair
* Height 176cm;
* Dark chain armour.

## Traits and other interesting things

* Pragmatic;
* Prideful;
* Sadistic;

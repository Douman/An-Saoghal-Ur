# Countries

## Saxons

Wolf people country.
It resides in west part of continent.

Their kingdom doesn't hold any particular name, but other
refers to them as just Saxons.

Current king: **Faol**.

## Holy Leanait Empire

_Note: leanailt - gaelic for act of following_

The only country with different culture among central inhabitants.
They culture is close to romanic.

Largest country on the continent.
Aggressive in its expansion, but at the moment of game timid due to internal conflicts.

Its ultimate goal is to spread Aonism through the world.
By any means necessary.

Currently have no emperor.
Previous emperor sudden death left country without a chosen successor.
There are two major candidates to the throne:

* **Morgause** - Oldest child and the only daughter of previous emperor. She is regarded as highly intelligent and her supporters believe she would be wise empress that would lead Empire to prosperity;
* **Kay** - Oldest son of previous emperor. Known for his military talents and beliefs. His supporters believe that with him Empire can continue its mission to spread their teachings.

There are more potential candidates, but they lack any significant supporters.

Contrary to other countries **Aonism** believes that anyone is suitable to rule
as long as they are of prophet descent.
Therefore women is considered equally worthy candidate.


## Yamato

Big island and country of eastern people.
It tends to isolate itself from mainland.

Currently has no emperor.
The only child of previous emperor, **Shizuno**, is too young to inherit and considered unsuitable as she is woman.
Therefore **Shizuno**'s mother acts as regent.

Due to archaic culture, it is believed that woman cannot rule and she should therefore marry.
Noble families seeks to influence empire household decision on **Shizuno**'s marriage partner.

### Shinto

Leader: **Kaguya**
Guardians: ???

### Conflicts

#### Main continent ban

Thanks to **Otomo** clan efforts contacts with continent are established.
But many clans of eastern part are still against it.

This is main source of conflicts among Yamato noble houses in which imperial family had been long ago caught.
Many noble households attempted to influence imperial family and currently they have the best chance by marrying their sons to princess.

### Influential clans

#### Otomo

Inspired by real [Otomo](https://en.wikipedia.org/wiki/%C5%8Ctomo_clan) clan.

Influential clan, thanks to alliance with **Shimazu**, that prospers from trade with main continent.
Due to their history of communication with foreigners, many eastern clans hold contempt against them.

Current leader: **Sorin**
Known to be a calculating pragmatic.

#### Shimazu

Inspired by real [Shimazu](https://en.wikipedia.org/wiki/Shimazu_clan) clan.

Influential clan whose leaders share imperial blood.
Despite that, they known to be loyal and never attempted to use their unique position (to overthrow imperial family).

Current leader: **Tadatsune**
Rightful, just and brave person.
Known to fight against corruption and law-breakers.
Despite that supports Otomo's idea to lift ban on trading with main continent.

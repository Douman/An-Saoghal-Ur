# Conflicts

## Aonism vs Old Faith

There multiple points of conflicts.

### Old Faith Gods

Aonism believes that there is only one god.
He created world and wish for people to strive to happiness.

It is of course contradicts to Old Faith beliefs.
As Old Faith beliefs in multiple gods have no place in Aonism.
Of course Aonism considers Sith and Old Faith Gods as only demons.

### Magic

Aonism teaches that magic is cursed art, and therefore can be learned only under guidance.
On other hand Old Faith followers, especially druids, are knows for magic practises.

Since magic users outside of Church are considered lost souls, Aonism seeks to hunt every one of them down.

## Yamato vs West

Yamato is mostly follow its isolation policy.
Due to that there is almost zero active conflicts.

Still historically Yamato has grudge against West people.
Mostly because ancestors of West people considered invaders and Sith followers.

### Invaders

Shinto teaches that demons (Sith) came together with their followers to take over the whole land.

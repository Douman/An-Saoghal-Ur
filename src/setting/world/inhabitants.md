# Inhabitants

## Humans

Major population of world.
In most regards the same as actual human race.

Except for eastern part of continent holds similar (european-like) culture.

### Central culture

European culture with mostly celtic roots.

Some pagans believe that they are successors to Sith.

Hold some contempt toward eastern people.

With decline of Old Faith started to hate beast people.

### Eastern culture

They call themself Yamato people and are part of only eastern country Yamato.
Based on Japan.

They tend to live peacefully with non-humans (whom they call yokai).

## Beast people

Humanoids with animal parts.

_Sith do not belong to this group_

No one knows from where they came from.

Even though they share similar to humans language & culture.
Most humans consider them as just monsters.
Due to that they tend to live away from humans and/or hold contempt toward them.

### Wolf people

Half-wolf kind that possess superior, to humans, physical abilities.
But has almost zero magic abilities.
Aggressive, cold toward non-wolf kind.

Their lifespan is similar of regular humans.

The only beast people that have own country.

#### Appearance

* Their skin covered by fur;
* Has fangs and claws.

### Harpies

Half-bird kind that lives mostly in mountains.
They posses wings and capable to fly.
Their magic abilities tend to evolve around wind element.

They are kind and air-headed.
Though still hold contempt toward humans.

Their lifespan is less of regular humans.

#### Appearance

* Has wings;
* Has bird-like legs;
* Their skin partly covered by feather.

### Kitsune

Their Appearance mostly human except for fox tail(s) and ears.
Posses high magic abilities and specializes in illusions.
Capable of hiding their beast attributes using magic.

Mostly known to live in eastern part of continent.

Their lifespan tends to be up to thousand years.

### Dragon kind

Almost non-existent by now.
They believed to be descendants of dragons.

They possess superior physical and magical abilities.
Believed to be immortal.

By now considered to be legends as dragons.

#### Appearance

Generally appearance may vary in all aspects.

* Their skin partly or fully covered by scales.
* May or may not posses wings and horns.

## Sith

Mystical people that are known mostly through legends.

While it is unknown what happened to them, they are known as previous masters of the world according to folklore.

No one among living knows if they exists.
Range of beliefs about this race is wide.
They are considered as demi-gods by pagan religions.

Common beliefs:
- They posses all kind of magic skills;
- Nearly immortal;
- They are categorized into two groups:
    - Seelie (Light) - believed to be mischievous, fair and benevolent. Represent summer and spring.
    - Unseelie (Darkness) - believed to be just, ruthless and cold. Represent winter and fall.

It is important to note that majority of pagans believe that Seelie is good, and Unseelie is bad (mostly due to folklore).

### Seelie court

"Light" kind of Sith.

Benevolent counterpart of Unseelie.
They share the same pride in their race and consider other races as lesser beings.
But they tend to guide and help, instead of dominating (contrary to Unseelie).
Often though they are mischievous, in their kind way.

#### Appearance

* Fair skin;
* Hair of white/bright colours.

### Unseelie court

"Dark" kind of Sith.

They consider themself, Sith, highest race.
While they mostly cold toward other races, they tend to instill respect, admiration and fear among lesser beings.
Even through display of force.
Tend to seek domination over lesser races

#### Appearance

* Dark\pale blue skin (close to obsidian);
* Hair of gray/dark colours.

# Magic

Generally magic is talent to use inner energy.
Firstly it requires talent, and only after that education and practise.

## Celtic traditions

Sith taught people how to manipulate mana and channel it to produce spells.

Magic can be of following elements:
* Fire;
* Wind;
* Water;
* Earth;
* Energy (neutral)

Contrary to common settings, magic itself doesn't require any additional actions.
As long as magic user is capable to channel magic through something(e.g. hands) he can use it.
Due to that magic is generally over-powered, but there are downsides of such usage of mana:
* Fatigue - by using magic without any special amplifiers mage wastes lots of mana to maintain spell;
    * Even with special amplifiers (e.g. wands) such magic requires lots of mana and only on rare occasions mage is capable to use many spells
    * Due to that AoE spells are rarely used, at least among humans as they have one of the lowest magic capabilities.
* Concentration - any use of magic requires significant amount of concentration. It is very easily to interrupt spell by striking caster.
    * One-shot/quick spells are more practical in warfare.
    * It is common for mage to have guardian(bodyguard).
* Strength depends on amount of channelling - i.e. the more time mage spends on channelling mana, the more powerful spell will be.
    * Rare AoE spells require at least few minutes to have a proper effect.

### Inner channelling

It is actually possible to channel inside body.
All spells can be target user itself, but it is still much less effective than channelling mana within user's body.
As mana is not directed toward external world there will be no wastes.

Not very known approach

It can be used to make user stronger, faster, thicker or attain regeneration abilities.

### Enchantment

Special runes(inherited from Sith) can be used to give weapon/armour magical enchantment.
It requires knowledge of runes and special materials to create rune that can sustain mana for long time.

All enchantments require constant or occasional, depending on type of enchantment, mana input to sustain itself.
Runes act more or less as a vessel to mana.
Different compositions of runes produces different effects.

## Yamato traditions

People of Yamato have a different traditions in regard of magic.
They do not use magic energy directly, instead they channel it into paper talismans.
The talisman, depending on how it has been created, can serve different purpose.

Comparing to usual magic, it relies heavily on caster education and will.

Types:
* Shikigami - turns talisman into shikigami that performs according to caster's will.
    * Follows any order.
    * Can be under direct control by caster.
        * When caster manipulates Shikigami directly, taking out him renders Shikigami useless.
* Summoning - Summon creature to fight for caster.
    * Contrary to Shikigami caster lacks ability of direct control;
    * More powerful type of magic;
    * It requires very high talent and knowledge;
    * There is a risk that summoned creature may turn against caster;
    * Death of caster will free creature completely;
    * Not widely used.

It is only used by Yamato people.

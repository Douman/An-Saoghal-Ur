# World

World consist of one continent and adjusted islands.

It doesn't bear any particular name.

Mainly inhabited by humans.

- [Countries](./countries.md)
- [Inhabitants](./inhabitants.md)
- [Religion](./religion.md)
- [Magic](./magic.md)
- [Conflicts](./conflicts.md)
- [History](./history/SUMMARY.md)

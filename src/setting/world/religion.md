# Religion

Pagan beliefs are based on celtic one.
The only monotheistic religion is based on Christianity.

## Aonism

Monotheistic belief into Aon, the only god of world.

It rejects belief that Sith are demi-gods and instead considers them as demons.
Due to that it holds contempt toward Old Faith.
While currently there are no active conflicts, in past Aonism has been known for its holy wars.

This religion serves as foundation of Holy Leanait Empire.

_Note: word 'Aon' comes from gaelic. Meaning One, Prominence_

### Prohet Naomhan

Naomhan, the first emperor of Holy Leanait Empire, is considered prophet.

It is believed that he has been blessed by Aon to lead people.
His bloodline considered holy and only one that has right to rule over people.
Due to that Leanait Empire is theocratic empire.

### Hierarchy

1. Aon - Father god that all must obey.
2. Holy Emperor - Descendant of prophet and the highest authority after Aon.
    * Approval of Aon is required to become  Emperor. But details are unknown to general public.
    * He is leader of both country and church.
3. Eminence - Highest position for any human (aside from Prophet descendants).
    * They are selected by Holy Emperor;
    * They are responsible to chose suitable candidate to be next Emperor.
    * They are dismissed with ascend of new Emperor.
    * The only role that do not require any church background, only approval of Emperor.
    * They do not govern church per se, but they are responsible for both nation and church policies.
    * They do not belong to church hierarchy itself.
4. Cardinal - Forms council to govern Church policies.
    * They both manages and represents Church.
    * They are responsible to execute Eminence orders.
    * Formally they respond only to Holy Emperor, but in reality Eminence can command them.
5. Bishop - Controls individual church, responsible to implement Cardinal Council policies.
    * They respond to their regional cardinal.
6. Priest - Practitioners of teachings.
    * Most priests belongs to particular church and therefore respond to church's bishop.
    * Mostly perform ceremonies such as funeral, marriage and etc.
    * It is allowed for priest to not belong a particular church, but they hold a little authority in that case.
7. Deacon - Learning priesthood. May also be assistant to the Priest.

While there are monasteries with monks, they strictly, ain't part of hierarchy.

#### Holy Knights

While belonging to church, holy knights respond only to Hole Emperor.
Their role is to be his sword and shield.

They do not play any role in church policies, but only serve as Holy Empire elite forces.

Holy Emperor is supreme commander of all military forces.

Any who possess significant talent to magic is forced to join Holy Knights though.
_Note: Generally many people possess some aptitude, but few are capable to become powerful wielders of magic_

1. Magister - leader of Holy Knights
    * He is directly respond only to Holy Emperor;
    * He is chosen among members of Round Table.
2. Round table - twelve most powerful knights.
    * Title is get by displaying your skills in all aspects of warfare.
    * Has right to doubt decisions and leadership of Magister.
3. Lord - commanding officer with own forces.
    * Typically receives title for achievements.
    * Granted by either Emperor, Magister or members of Round Table.
4. Knight - requires two years training.
    * In rare cases it can be awarded immediately, if one accomplish significant deeds.
5. Apprentice - initial rank upon joining.

### Mission

#### Creeds

* Belief in Aon, the holy father;
* Prophet Naomhan and his descendants rule in his name;
* Soul purity:
    * Upon death, if soul is sinful, it will go to hell to be cleansed.
        * In some cases soul cannot be cleansed and it is doomed to suffer forever.
    * Negative emotion, evil deeds and cardinal sins make your soul dirty.
    * Clean soul ascend to heaven.
* Polytheistic gods are devils that seduce people away from living a good life.

#### Teachings

* Be a better person - mostly implies to love neighbour and avoiding cardinal sins;
* No self-indulgence - Humans cannot clean themself of sins. They are going in hell for that.
* Teach non-believers - Each believer should teach non-believer about Aon and its way of life.

#### Cardinal sins

* Lust;
* Gluttony;
* Greed;
* Sloth;
* Wrath;
* Envy;
* Pride

#### Magic

Magic is considered a cursed art and so it can be used only under guidance.
It is believed that demons (Sith) taught art of magic, in order to spread chaos.
Therefore Church requires all with talent in magic to enter ranks of Church or Holy Knights.

All magic users without guidance are believed to be corrupted and are being hunted.

## Old Faith

Polytheistic beliefs.
Similar to irish(celtic) beliefs.
Contrary to Irish myths, Sith and humans came to this land together.

Became called like that after appearance of new religion.

Old Faith consist of multiple beliefs.
It may differ from one region to another.

There are though some common things.

### Hierarchy

There is no hierarchy but druids are considered carries of old wisdom and faith.
Therefore they are revered like priests.

### Gods

The same pantheon is shared among all believers.

It is believed that Sith are positive force.
While Fomorians are negative force.

#### Danu

Mother goddess.
She believed to be creator of land and people.

#### Nuada

The god of sea, healing and warfare.
Believed to be the first king of the Sith.

He is the one who brought people to the land and won over Fir Bolg.

#### Lugh

God of sun and sky.
He is revered for defeating Balor, the king of Fomorians.

#### Morrigu

Triple goddess which is known as Phantom Queen.
It is believed that Morrigu is goddess of war, sovereignty and fertility.
Legends say that she has three different form depending of what she represents(war, sovereignty or fertility).

## Shinto

Beliefs of eastern people.
Based on real-life Shinto.

According to legends Yamato is created by ancient gods (Izanami & Izanagi) who lived before humans.
Yamato's imperial bloodline is believed to be from legendary Amaterasu, daughter of ancient gods.

While they acknowledge existence of gods, they do not worship them.
Instead they are considered to be just part of nature.

It is also believed that Sith are demons which invaded world in past.
Due to that Sith are considered enemies to the Yamato.
Therefore Old Faith is considered harmful and its eradication is promoted.

### Mission

It's goal is to protect Yamato and preserve its people.

Similarly to Old Faith it doesn't hold strong teachings for how people should live their lives.
It is though promotes to love neighbour and be a better person.

#### Purity

Shinto believes in concept of purity.
Similarly to Aonism it is believed that negative emotions and evil deeds may make you impure.
But Contrary to the Aonism people can purify themself through rituals.

There is no strong stance against impurity.
Shinto though teaches that keeping yourself purified, leads to happiness.

### Hierarchy

There is almost zero hierarchy.

* Leader - Mostly nominal leader.
    * While leader figure may issue decrees to all followers, it is not mandatory to follow.
* Guardians - Selected mages that are responsible to guard Yamato from threat of demons.
* Priest - Either responsible for shrines or teaching of people.

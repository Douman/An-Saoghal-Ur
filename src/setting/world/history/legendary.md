# Legendary History

The history here covers _known_ pre-recorded history of the world.
As current civilization has no records of this period, only few people even aware of details.

Notably Yamato knows their history very well and as it traces back to Celts arrival they are mostly
the only one who has some records of this period.
But even they have no details of history before as their civilization were completely wiped out by Sith.

### Celts arrival

Sith and human allies arrived to the land.
The land itself was huge, but it was already populated.

People here was called Fir Bolg.
**Note:** Fir Bolg is actually not race, but just name of people in west region.

At first Celts tried to negotiate with them so that they could allowed to settle.
But due to constant conflicts among themself, Fir Bolg quickly rejected all attempts.

**Nuada** then declared war on them and defeated Fir Bolg army pretty quickly.
Thanks to magic and superior equipment his army got a pretty easy victory.

#### Celts Hierarchy

Human allies are the one who are Celts.
Since they were numerous comparing to Sith, invasion army were referred as Celts.

Also they were actually slaves and servants of Sith.

### Celts settlement

After initial win, Sith got much more of land they hoped for.

It raised a problem though as their numbers weren't that big to manage it.

At this time some of defeated Fir Bolg started to worship them.
They were afraid of magic and thought of them as gods.

Sith used this opportunity and started to send out new followers with their servants.
Pretty quickly people of their new land started to worship Sith as gods.

### Celts carries on

Realizing that their magic could be used to bend the whole continent.
As many humans kneels before their superiority, Sith themself start to believe that they need to rule humans.

Again thanks to their magic skills, Sith quickly defeated one army after another.
Even superior numbers of Fir Bolg couldn't help against magic.

### Celts and Fomorians contact.

Continuous conquest of Fir Bolg lands led Celts to meet Fomorians at the south.

**Note:** At the moment of game, there is no historic records of what exactly they, Fomorians, are.

To Sith surprise, Fomorians possessed magic too and were far superior to Fir Bolg.
Turns out that Fomorians were few in numbers, but far more brutal in dealing with Fir Bolg.
Fir Bolg called them demons due to their appearance being partly animal.

At the moment Sith were unaware of Fomorians origin as they were spread among Fir Bolg lands.
In most cases they were enslaving and burning Fir Bolg villages.

While Fomorians proved to be strong opponents, Sith prevailed over their bands.
They though started to hear rumors about demon king Balor.

### Fir Bolg last stand

It took hundred years to completely conquer south and central part of continent.

By the end, most Fir Bolg kingdoms were surrendering peacefully and welcomed Sith as gods.

There was still last obstacle to complete conquest.
At the east was large kingdom that occupied the whole east coast of continent.

Initial skirmishes proved that Celts would require much more efforts to defeat them.
Fir bolg were leaded by three siblings: Amaterasu, Tsukuyomi and Susanoo.

**Note:** While Shinto claim them being gods of past, there is no information of who they are actually.

The Sith were at first surprised by might of siblings army, but eventually Celts gained upper hand.
It was though time when Nuada lost his hand in battle against Susanoo.

The fight was long and tedious, and at that time negotiations began between Fir Bolg and Celts.
Among Celt's delegation there was Bres, that demanded Fir Bolg to surrender half of their territory.
Fir Bolg rejected his demand and continued to fight long and tedious war.

Despite their best efforts, causing heavy losses among Sith, eventually Fir Bolg are left with small portion of their territory.
A truce is called, and Fir Bolg are given options: leave land, surrender or fight.
Even though fighting means eventual defeat, Fir Bolg rejects other options.

Susanoo challenges Nuada to fight instead of spilling even more blood..
But Nuada agrees to duel only on condition that Susanoo should fight only with one hand.
Which is rejected and battle carries one.

Eventually Sith were so impressed with Fir Bolg that they proposed a peace treaty.
Celts offered them land and promise to not fight anymore.
Tsukuyomi and Susanoo agrees, but Amaterasu is afraid that Sith would use other means to enslave their people.
She proposes to take Yamato region and split it from continent.

_Exact details are not known even to Yamato people_

This gives birth of Yamato island.

### Rise of Fomorian King

#### New king Bres

With Nuada losing his hand, Sith started to consider who will become new king.
Gravely maimed Nuada no longer fit to be the king.

It was then Bres took throne by swaying majority of Sith to his side.
But he proved to be a tyrant and his reign had been known as shortest among Sith kings.

Not only he treated human followers as trash, but even Sith themself were treated poorly.

Meanwhile, thanks to Dian Cecht, later knows as god of healing, Nuada replaced lost hand with silver one.
Nuada, then reclaimed throne and Bres has been exiled.

### Bres seeks revenge

According to legends Bres was half-Fomorian and therefor seek land of his Fomorian ancestors.
He grew hateful toward his Sith origin and wanted to re-take throne by force.

Sadly for him, his own Fomorian father rejected his plea as foolish.
But another Fomorian lord was ready to help.
His name was Balor.

_It is not known, but Fomorian had large alliance of kingdoms across multiple islands south to the continent_

### Fomorian army invasion

With the aid of Balor, Bres attempted to take Sith throne by force.

War against Fomorians were far more dreadful even in comparison with Fir Bolg's last stand.
Fomorians were wild, strong and numerous.
It was dark times for Sith, until Lugh, another half-Fomorian haven't come to join Nuada.

At first Nuada was reluctant and distrustful toward him, but eventually he saw him as hero.
Hero that could end Fomorian threat.

Nuada wasn't able to see end to Fomorian invasion as he has been slain by Balor.
Lugh kills the Fomorian leader with his sling, smashing his deadly eye through the back of his head where it wreaks havoc on the Fomorian ranks.

Loss of their leader led to dismantle of Fomorian army and eventual scattering among land in fear of Sith revenge.

**Note:** Beast people might be Fomorians?

### Legendary Sith reign

After defeat of Fomorians, Sith became sole rules of continent.
Exception was Yamato islands Sith had no intention to conquer.

This time has been knows as golden age of Old Faith and Magic Arts.

Sith established dynasty and ruled over humans with absolute power.
While humans saw them as demi-gods, Sith considered them as lesser beings.
And due to a few numbers, it was rare for human to see Sith.

There is no historic records about length of this period.
Known to be approximately close to thousand years.

#### Yamato isolation

After separating Yamato from content, its people ceased all contacts with continent.

God-like siblings were praised and people asked them to lead.
But Tsukuyomi and Susanoo stepped out of this privilege.
Amaterasu became sole ruler of Yamato.

She proclaimed that demons conquered continent and they can no longer have any ties to it.
It started long isolation of Yamato.

##### Shinto teachings

Many in Yamato viewed previous war as catastrophe.
There were these who were afraid that demons might come after them eventually.

And at this time Amaterasu's brother Tsukuyomi started his journey around Yamato.
Knowing that eventual contact with invaders will happen, he wished to teach people how to counter magic.
For many, magic was almost god-like art and it was one of the reasons why many believed that Sith are demons.
Tsukuyomi was among few who were capable of magic feats due to his talent.
Knowing that magic requires inherit talent, he started to work on dealing with this limitation.

Meanwhile he was on journey through Yamato island.
During this journey he taught people of good and demons.
His teachings became basis of Shinto.
But as Shinto isn't organized religion, but rather part of culture.
Details of its creation ain't known.
Within hundred years Shinto traditions became known.

#### Dominance of celtic culture

With Yamato people avoiding any contacts, and Fomorians being no longer a threat.
Celtic culture eventually became dominant within few hundred years.
But due celts being minority, they adopted Fir Bolg language.

While eventually there were some regional differences, all people shared language and culture.

#### Destruction of Fomorian islands

While Fomorian army has been defeated, they still reign over multiple islands to south.

Eventually Sith decided to conquer these islands and dismantle Fomorian kingdoms
With this Fomorians were no more and forgotten.

### Dark ages

This period is known as dark ages by both followers of Old Faith and Aonism

#### Yamato peace

With no more invasion and threat of demons, people started to forget about it.
It was still known that outer world is occupied by demons, but people firmly believed that they are safe on island.
All communication with main land was prohibited.

Amaterasu established ruling dynasty and her descendants became first Emperors
Eventual disappearance of god-like siblings led many to believe that they were in fact gods that protected people of Yamato.
And these gods went to Heaven, upper world.

Before his disappearance, Tsukuyomi established new magic tradition.
He taught people how to create special talismans and turn it into shikigami that would follow caster's will.
His students became known as Yamato Guardians since Tsukuyomi emphasised importance of magic in fight against demons.
Special talismans creation required long and tedious education, but it allowed even less talented people to be able use magic.
Though it didn't change importance of magic talent, as it directly impacted power of talismans.

#### Sith civil war

Sith rule eventually started to crumble due to inner fightings.
This was the time when Sith split into Seelie and Unseelie.

But contrary to world's modern beliefs, they were split basically between two candidates to throne:

* Actair, Smiling Prince - kind and benevolent Sith. He attempted to reclaim throne from White Lady, as he found her leadership lacking.
    * He disliked policies of **Lurdan**, that used his niece absence to his advantage.
    * He recruited humans to his army and therefore had advantage in numbers.
    * He had contempt toward White Lady for complete disregard of her duties.
    * Knowns to be pretty mischievous and due to that enraged White Lady.
* Ciarnight, White Lady - Sith sage that inherited throne without any desire to rule. She spent most of her time in seclusion away from her duties.
    * Her uncle, **Lurdan**, used her absence to rule in her steed. He was known to be ruthless.
    * She genuinely was uninterested in ruling and that provoked Smiling Prince to challenge her.
    * As she wasn't interested she was ready to give up throne, but due to shame Prince brought on her, she sought to defeat him.
    * Her, or actually her uncle, supporters were mostly against treating humans as equals. They considered Smiling Prince ideas as dangerous.

**Note:** Intentional contradiction to real mythology behind Sith.

White Lady hadn't abounded her duties immediately.
While she wasn't interested, she still attended her duties properly.

But one day she disappeared for few years.
It made some confusion but thanks to her uncle **Lurdan** nothing serious happened.
He took her duties by claiming that his niece left it to him temporarily.
While some suspected a lie, there wasn't much of opposition.

Everything changed when White Lady had returned.
She didn't explain anything and just confirmed her uncle words.
After that she almost immediately disappeared again.

This time disregard of queen's duties started some unrest among Sith.
Most notably **Actair** claimed that queen cannot ignore her duties like that.
Being her brother (from different mother) he asked **Lurdan** where to find queen.
His goal was to challenge her, but **Lurdan** disregarded him and told to just wait.
Which only fuelled **Actair** more.

To make matters worse, unrest among humans became worse.
There were always these who challenged Sith rule, but recent years brought full-blown rebellions.
Mostly due to complete lack of Sith control, humans started to rule themself and ignore Sith masters.
When Sith started to notice that humans are disregarding them, **Lurdan** quickly organized army to raid all self-proclaimed warlords.
**Lurdan** had been also known for thinking of humans as no more than slaves.
Treatment toward humans from their Sith patrons became worse during his reign.

Many, among Sith, found **Lurdan** crude and foolish.
Among them was also **Actair**.
He believed that humans had full right to self-govern when their own Queen is ignoring her duties.
And **Lurdan** is treating them as just property.
Seeing this opportunity he started to gather human followers with promise to overthrow tyrant.
And if he would become king, he wouldn't let his subject being treated as slaves.
With that many among humans went under his banner to rebel.
These, among Sith, who hold contempt toward **Lurdan** also supported Smiling Prince.

At the time of this escalation, **Ciarnight** had returned.
To only find that her uncle and **Actair** were getting ready to war.
While she was trying to understand what's going on, **Actair** saw opportunity to challenge her directly.

He accused her of irresponsibility and demanded to surrender throne.
And he did it in front of court.
There was no way to just dismiss him without consequences.
While she understood that, for some reason she just disregarded him and promised to deal with her uncle.
**Actair** had been thrown out to only gather his allies for fight.

Completely ignoring him swayed majority of her, or to be more precise her uncle, supporters toward **Actair**.
Which at the moment wasn't her major concern.
It is unknown what was the reason for her careless disregard.
She also exiled **Lurdan** which left her basically with no support of nobility.

Despite that she was able to quickly prepare for **Actair** army.
While she lacked support of nobility and her army wasn't so numerous.
She was able to balance it out by superior tactics.
Thanks to her victories she was able to sway back some support from nobility.
Which strengthen her forces and allowed her to be on more equal terms.

#### Prophet rise

At this time young lad by name **Naomhan**, started his journey to unite people against waging war Sith.
He believed that people should start govern themself instead of leaving it to self-proclaimed gods.
While **Actair** and **Ciarnight** fought among themself they paid almost no attention to his rise.
Thanks to that **Naomhan** was steadily gaining supporters.

Within few years he gathered many followers and successfully established base at far south.
Even fighting Sith armies started to consider **Naomhan** a major threat.
But to everyone surprise **Naomhan** didn't try to move any further.
Instead he just hold and continued to welcome any new followers.
While some were dissatisfied with his decision among his followers, it made Sith less cautious about him.
**Actair** disregarded him as another petty lord and pretty quickly rejected **Ciarnight** attempts to negotiate.

#### Yamato internal tension

While communication with continent was prohibited, there were some who ignored imperial law.
One of them was noble house **Otomo** who from time to time send their spies to continent.
With time even Imperial house became aware of it, but they lacked concrete evidence.
Current weakness of imperial family also made it difficult to punish **Otomo**.
Many noble houses supported **Otomo** as they believed information about continent may prove to be useful.

On other hand there were houses who hold contempt for breaking sacred law.
Many saw unpunished **Otomo** as sign of imperial family corruption and weakness.

Meanwhile Emperor **Shun** was very secluded and mostly imperial ministers managed all affairs.
Often these ministers had own agenda and there were these who covered for **Otomo** clan.
During this time Emperor's authority was becoming lesser with years, while individual noble houses authority was raising.

Eventually it would lead to prolonged period of internal wars among these noble houses.

#### Sith civil war stalemate

Regardless of how superior tactics of **Ciarnight** were, she couldn't defeat superior numbers of **Actair**.
To make matters worse she needed to divert part of her forces to deal with rebels.
Establishment of **Naomhan** power brought more and more instability.
Thanks to her uncle, many people saw Sith as abusers rather than gods and rightful rulers.
Civil war among Sith only fuelled it even more.

**Actair** on other hand considered **Naomhan** as good way to weaken **Ciarnight**.
He believed that after victory, crush of **Naomhan** would bring peace.
But contrary to his expectations, **Ciarnight** forces were able to withstand even with diverting forces to suppress rebels.
Eventually even his forces started suffer from **Naomhan** influence as his human followers started to desert.

#### Prophet meets Aon's messenger.

With stead gain of supporters **Naomhan** started to consider his next move.
Sith fighting among themself was a golden opportunity, but he had doubts about chances to win.
After all, even with all his followers, he couldn't stand against Sith magic.

Even most talented mages among his fellows were no match to Sith.
Sith magic could destroy his army in an instant.

It was changed in an instant when he met her...

One day many **Naomhan** followers gathered to see their leader.
Many were impatient while others were afraid of being forced to serve Sith again.
At this day **Naomhan** appeared in front of them as usual, but this time right behind was lone lass.
She had beautiful grey tail and wolf ears.
It caused some tension, as beast people were believed to be a mere monster.
But quickly all this tension disappeared when **Naomhan** declared holy war on Sith.

That wasn't the only thing he spoke of.
He spoke of his companion being messenger of true god Aon, of his teaching and how Sith were vile demons.
Some might have doubts about true god, but most of his followers cheered his message.
With foundation of Aonism, long waited fight against Sith had started.

#### Sith doubts

While civil war was raging on, there were rumours among Sith that current times are sign of upcoming disaster.
With war having no clear superiority, it became nuisance even to long-lived Sith.
They might live long, but during course of war their kind became rare, as many died in war.
And while there was no shortage of human soldiers, same couldn't be said about Sith.

With that both **Ciarnight** and **Actair** eventually lost support of Sith noble houses.
Leaving both of them with mostly human soldiers.
Sith law also allowed to not participate in such feuds among Sith themself.
And that was exactly how war was viewed by noble houses.

Without support of noble houses, both armies had shortage of mages.
Which of course was good to **Naomhan**'s cause.

#### Holy war begins

To be precise, actual **Naomhan** war against Sith started only few years after his declaration.
**Naomhan** close followers explained that delay by making preparations.
While in most cases it worked, there was occasion of self-proclaimed warlords rallying people.
These were rare occasions though.

The main problem for **Naomhan** was a magical superiority of Sith.
Even knowing, thanks to his spies, that Sith noble houses backed out of both armies, he was still seeking means to counter magic.
Eventually he would need deal with current Sith ruler **Ciarnight** who has been known as most powerful sage.

At that time divine messenger (her name has been never revealed) gave **Naomhan** means to counter magic.
She spread knowledge of magical crafting and enhancements.
While only few were able to master these arts, it gave **Naomhan** army counter measures against magic.

With that **Naomhan**'s army split into two and each went into opposite direction: east and west.
Both armies were aiming to avoid direct confrontation at the beginning and instead focus on recruiting as much people as possible.
While they were successful at first, eventually armies became hordes that couldn't be controlled properly.
And some uncontrolled parts started to plunder and pillage which attracted Sith attention even more quickly.
It took some time, but eventually **Ciarnight** was able to persuade **Actair** to conclude truce.
With that Sith armies were able to start dealing with **Naomhan** forces properly

#### Yamato invasion

Meanwhile with Emperor losing its authority, individual noble houses were looking for opportunities.
**Otomo** and **Shimazu** houses shared with every other noble houses news of internal fights among demons (Sith).
They started persuading everyone that it was a chance to take back rightful territories. Including Emperor.
While authority of Emperor became almost zero, his blessing would still inspire many.

While some eastern noble houses opposed the idea, majority was in favour of it.
And so was Emperor's son **Takeshi** who wanted personally lead people.
Thanks to him, **Otomo** and **Shimazu** got support of Emperor and many noble houses.
Emperor himself saw it as opportunity for imperial house to rise its prestige.

With that Yamato quickly started preparing army.

#### Rebels losing ground

Having concluded truce with **Actair**, **Ciarnight** decided to quickly crush rebellious forces.
Even with superiority of Sith forces, they were not numerous and many among human soldiers were easily swayed from duty.
While not many dared to openly betray, some were unwilling to rise weapon against their own kind.
Due to that **Ciarnight** mostly used core army that mostly consisted of Sith and magically enhanced humans.

Knowing that **Actair** was able to sway many to his side, she hoped to use his army to corner rebels.
At first **Actair** did follow her plan and **Naomhan** forces were slowly losing its ground.
Eventually it started to become impossible for rebels to maintain such big army.
**Naomhan** became desperate and at that time envoy of **Actair** had approached him.

**Actair** proposed alliance against **Ciarnight**.
He wished to use **Naomhan** army as bait to lure **Ciarnight** away from main forces.
Of course it was up to **Naomhan** to devise plan for that, but if it would be possible then **Actair** himself kill her away from unwanted gazes.
There was certain fear of it being ploy, but at that time Aon's messenger approved of this plan and even suggested means to lure **Ciarnight**.
While some doubts remained **Naomhan** agreed to make his army a bait for **Ciarnight**.

_Means to lure Ciarnight are not known to history_

At that time **Naomhan** forces were slowly retreating to south of continent.
**Actair** on other hand intentionally slowed his forces despite **Ciarnight** orders.
**Ciarnight**,even though being wary of **Actair**, was not expecting any sort of ploy.
Despite being advised of caution, she marched her forces with the same pace.

_Reason of rash is that Aon's messenger appeared before her army and tried to assasinate her using Fomorian magic_

Sadly for **Actair** he had no time to enjoy butchering his queen.
Sudden news of Fir Bolg invasion from their secluded islands forced **Ciarnight** to retreat.
While sudden retreat of Sith forces were surprise it gave **Naomhan** forces some time to rest.

Thanks to magic, Sith forces were able to quickly reach east coast.
But it was leaving issue of rebels.
Knowing that she cannot leave, **Actair** suggested to take his army to personally deal with invasion.
While also assuring that **Ciarnight** has more than enough forces to keep rebels at bay and wait for his return.

#### Yamato success

Initially Yamato army had almost zero troubles to conquer eastern coast.
With that they were able to fortify their positions around coast and vent further into continent.
As **Takeshi** was aware of current conflicts among Sith themself and humans, he hoped to become hero of enslaved by Sith people.
He sought to send out envoys before approaching cities and villages.
It considerably slowed down their progress but **Takeshi** hoped to persuade people to join instead of conquering them.

Sadly for him it didn't went so well.
Many among continent residents were afraid of new invaders.
Some even considered it divine punishment for betraying Sith.
**Takeshi** good will back-fired by actually solidifying loyalty to Sith over eastern part of continent.

Eventually **Takeshi** had been persuaded by his generals to let army be more aggressive.

Initially general plan was to conquer around historical borders of Yamato kingdom.
According to old maps they were around mountain range which would serve as good for repelling eventual Sith counter-attack.
Unexpectedly Sith forces were already approaching before Yamato army was able even to reach half of the way.

_Yamato had no idea of actual magical capabilities of Sith. Especially teleportation magic_

After long discussion it was decided to battle Sith army.
Knowing that they were most likely wearied by internal conflicts many believed that it would be better to strike immediately.

#### Actair strikes

Initial skirmishes proved superiority of Yamato forces.
There were mostly no mages among Sith forces and shikigami of Yamato mages were able to instil fear.
But it didn't last for long as it was only vanguard of Sith army that **Actair** used to study Yamato forces.
As Yamato army was gradually pushing forward through Sith vanguard away from coast, **Actair** transported his army to eastern coast.
With that he quickly captured coast and severed any way for supplies to reach Yamato army.

Catching news of Sith forces capturing eastern coast Yamato army was thrown in confuse.
No one could imagine that Sith forces could appear far behind their army.

_While technically Sith can teleport anywhere, for big army they require lots of magic power and stone circle as its destination.
Due to that there is no way to Sith to teleport armies to Yamato islands. Even individual teleportation is very difficult without anchor as stone circle._

_Another thing noteworthy is that Sith does not teleport, instead they travel through another dimension that exist in parallel. While they are there time is stopped._

Unwilling to simple show back to Sith vanguard forces, **Takeshi** asks mages to unleash summoned creatures.
There was some risk as summoned creates eventually break free, but other options would be to split army.
Being afraid of other tricks, unleashing summoned creatures seemed the only choice.

#### Summoning magic first show case

Yamato forces hoped that unleashed creatures would stop Sith vanguard and eventually defeated by Sith forces, but they turned to be even more powerful.
There were many dreadful creatures that started to ravage land while Yamato forces were retreating to eastern coast.
Rumours of invaders unleashing demonic creatures spread among Sith lands very quickly as **Actair**'s vanguard forces were wiped out quickly.
With Yamato army quickly approaching his positions he didn't wish to take care of this problem though.
Instead he urged **Ciarnight** to sent capable mages to destroy summoned creatures while dealing with Yamato army.

Even though she couldn't let Fomorian go, she also couldn't ignore ravaging magical creatures.
She decided personally deal with them while taking only small elite forces with her.
**Ciarnight** was the only Sith mage that could constantly travel through another dimension without need for stone circles.
Due to that she was able quickly start disposing of summoned creatures.
But she not only were disposing of them, in addition she found out that the one capable of summoning could bring a even greater deal of harm.
So she sought to wipe out anyone capable to summon.

_Techincally summoning magic allows to bring creature from another dimension._
_There is also no limit what sort of creature you can bring, therefore it is easy to unleash hell by bringing something powerful._
_Like something that urged Sith to come to this world..._

Catching wind of **Ciarnight** venture to deal with summoning creatures, **Actair** suspected that she's going to unleash powerful magic to wipe out Yamato army.
He didn't want to end it quickly and instead marched to meet Yamato forces.
Meanwhile he sent envoy to **Naomhan** using 'teleportation'.
His hope was to persuade **Naomhan** with his otherworldly ally to strike **Ciarnight**.

_Actair was fully aware of Naomhan ally from the beginning_

#### Dreadful light

Once **Ciarnight** understood that summoning creatures are bind to actual summoners she went to catch up with Yamato army.
She decided to destroy them using most powerful spell that she could think, alongside with all living there.
It seemed as little sacrifice to stop ravaging monsters as there were too many of them.

Despite wish to kill **Actair** alongside she didn't wish his army to perish.
While she was preparing the spell, she sent envoy to warn **Actair** and ask him to retreat.
Meanwhile **Naomhan** was on his way with his ally using Sith help.

_No one actually knows what happened except for Ciarnight_

After that sudden light that illuminated the whole continent on night.
Many believes it was result of **Ciarnight** dreadful magic that destroyed both Yamato army and **Actair** forces.
Regardless it transformed eastern coast into barren crystallized wasteland.

Eventually **Naomhan** returned to his people and proclaimed that both Sith tyrants were slain and now people can be free.

#### Sith rule crumbled

Having no leaders and no armies, Sith were quickly overthrown by either **Naomhan** forces or other rebellious humans.
Any Sith follower was executed or enslaved.
True dark ages come to land with humans destroying every remnants of Sith in fear of something like **Dreadful light** happening again.
It was especially hard for mages as they were dreadfully feared, regardless of their race.

While chaos was ravaging continent **Naomhan** started firmly establishing his own empire.
Many urged him to unite world but he was reluctant to do and instead established his rule across south and central part of continent.
With that **Holy Leanait Empire** has been established.

#### Yamato civil war

Losing all contacts with their army, many believed start sudden light was devastating punishment by Sith.
Many started to blame **Otomo** clan and Emperor for breaking the isolation.
Massive loss of army, and even Emperor's son, strongly solidified belief that Yamato should continue its isolation.
Clans that were not willing to participate in invasion now had armies to dictate their will and many raised to take control of country.
This started prolonged civil war among Yamato noble clans.

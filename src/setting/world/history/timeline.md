# Timeline

Starts with Celts arrival.
While technically there is history  prior to it, it is completely lost.
Due to destruction of old civilization by Sith.

## Legendary times

Sith arrival is starting point and all other events are relative to its time.

* ??? - Sith appears with Celts on the continent.
* ~10 year - West continent is completely conquered by Sith.
* ~150 year - Sith carries new conquest further east and south.
* ~180 year - Sith meets Fomorians and defeats their small kingdom at south.
* ~250 year - Sith conquered majority of continent. Last Fir Bolg kingdom is left at east coast.
* ~252 year - First battles against last Fir Bolg kingdom.
* ~253 year - **Nuada** loses hand in fight against **Susanoo**.
* ~255 year - First attempt to negotiate peace treaty.
* ~260 year - Fir Bolg loses almost all territory.
* ~261 year - Sith offers peace and biggest region of Fir Bolg kingdom, known as Yamato. Eventually it becomes Yamato islands.

* ~262 year - **Bres** becomes king of Sith.
* ~270 year - **Dian Cecht** replaced Nuada hand with silver one.
* ~275 year - **Nuada** reclaimed throne with **Bres** being exiled.
* ~290 year - **Bres** acquired help of **Balor**.
* ~320 year - Fomorian invasion of continent.

* ~360 year - Shinto traditions founded in Yamato.

* ~400 year - **Lugh**, half-Fomorian, joins **Nuada**.
* ~515 year - **Nuada** is slain by **Balor**.
* ~520 year - **Lugh** becomes leader of Sith armies.
* ~550 year - **Balor** is slain by **Lugh** and Fomorians are scattering across land to hide from Sith.
* ~555 year - **Lugh** becomes Sith king and golden age of Sith starts.

* ~600 year - **Amaterasu** peacefully united Yamato islands and established ruling dynasty.
* ~700 year - **Tsukuyomi** founded new magic traditions in Yamato.

* ~700 year - Celtic culture becomes dominant. Fir Bolg language becomes adopted by Sith kingdom.
* ~1250 year - Fomorian islands are raided and devastated by Sith.

* ~1800 year - Otomo clan makes first ventures to continent.

* ~1900 year - Dark ages.
* ~1950 year - **Ciarnight** disappearance.
* ~1955 year - **Ciarnight** second disappearance.
* ~1960 year - **Lurdan** purge of humans.
* ~1970 year - **Actair** gathers his followers and forms own army.
* ~1975 year - **Ciarnight** returns and **Actair** challenges her. Eventually **Lurdan** is exiled.
* ~1980 year - Civil war starts.

* ~2000 year - **Naomhan** ventures through continent to gather allies.
* ~2020 year - Aon's messenger meets **Naomhan**
* ~2025 year - **Naomhan** starts holy war.
* ~2030 year - **Ciarnight** and **Actair** concludes truce.
* ~2035 year - Yamato invasion.
* ~2040 year - Day of Dreadful light.
* ~2041 year - Yamato civil war starts.
* ~2050 year - Holy Leanait Empire is established. Leanait calendar established.

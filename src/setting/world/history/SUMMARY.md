# History

World history, legends and timeline.

- [Timeline](./timeline.md)
- [Legendary](./legendary.md) - Pre-recorded period. People know this period only through legends and folklore.

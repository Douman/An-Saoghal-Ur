# setting

Describes world and details of setting.

- [Characters](./characters/SUMMARY.md)
- [Map](./map/SUMMARY.md)
- [World](./world/SUMMARY.md)

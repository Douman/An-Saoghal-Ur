# Story plan

* [Prologue](#Prologue)
* [Chapter 1](#Chapter 1)

## Prologue

[Content](1-Prologue)

#### Introduction of main character

Initially MC lives at monastery since he has been abandoned from birth:

* Short prologue into his life and kinda world view.
* His life consist of mostly studying in hope to become travelling priest.
* As monastery is one of the main source of education, MC is relatively knowledgable.
* There should also introduction of fatherly figure that MC had.

##### Expedition

As part of prologue MC will travel to north

##### Meeting White Lady

**Ciarnight** should be introduced as mysterious magician during expedition

#### Attack

* Monastery gets attacked by some strange monters.
* Meeting with **Morgause**
* Either go with her or die?

## Chapter 1

[Content](/2-Chapter_1)

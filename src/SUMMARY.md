# Summary

* [About](./about.md)
* [Story](./story/SUMMARY.md)
    - [Prologue](./story/1-Prologue/SUMMARY.md)
        - [Introduction](./story/1-Prologue/1-Introduction.md)
* [Setting](./setting/SUMMARY.md)
    - [World](./setting/world/SUMMARY.md)
        - [Countries](./setting/world/countries.md)
        - [Inhabitants](./setting/world/inhabitants.md)
        - [Religion](./setting/world/religion.md)
        - [Magic](./setting/world/magic.md)
        - [Conflicts](./setting/world/conflicts.md)
        - [History](./setting/world/history/SUMMARY.md)
            - [Timeline](./setting/world/history/timeline.md)
            - [Legendary](./setting/world/history/legendary.md)
    - [Characters](./setting/characters/SUMMARY.md)
        - [Ciarnight](./setting/characters/ciarnight.md)
        - [Feallair](./setting/characters/feallair.md)
        - [Kaguya](./setting/characters/kaguya.md)
        - [Morgause](./setting/characters/morgause.md)
        - [Shinobu](./setting/characters/Shinobu.md)
        - [Shizuno](./setting/characters/shizuno.md)
